node-static-module (2.2.5+~cs4.2.3-1) unstable; urgency=medium

  * Team upload

  [ lintian-brush ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Fix field name case in debian/copyright (comment => Comment).

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on node-duplexer2,
      node-lodash-packages, node-resolve, node-through2.
    + node-static-module: Drop versioned constraint on node-duplexer2,
      node-lodash-packages, node-through2, nodejs in Depends.

  [ Yadd ]
  * Bump debhelper compatibility level to 13
  * Add "Rules-Requires-Root: no"
  * Add debian/gbp.conf
  * Modernize debian/watch
  * Use dh-sequence-nodejs auto test & install
  * Fix GitHub tags regex
  * Update standards version to 4.6.0, no changes needed.
  * Fix filenamemangle
  * Drop dependency to nodejs
  * Drop unneeded versioned constraints
  * Embed components: estree-is-function merge-source-map scope-analyzer
    shallow-copy
  * Drop patches
  * New upstream version 2.2.5+~cs4.2.3
  * Embed from2-string for test only
  * Update dependencies:
    + remove node-lodash-packages, node-quote-stream
    + add node-acorn, node-array-from, node-convert-source-map,
      node-debbundle-insert-module-globals, node-es6-map, node-es6-set,
      node-es6-symbol, node-magic-string, node-readable-stream,
      node-source-map

 -- Yadd <yadd@debian.org>  Sun, 14 Nov 2021 09:06:23 +0100

node-static-module (2.1.1-1) unstable; urgency=medium

  * New upstream version
  * Drop patch for using lodash has. Use has
  * Bump compat and policy
  * Move to salsa

 -- Bastien Roucariès <rouca@debian.org>  Tue, 05 Jun 2018 22:12:10 +0200

node-static-module (1.5.0-1) unstable; urgency=high

  * Security bug fix: Uninitialized Memory Exposure
    (Closes: #863480).

 -- Bastien Roucariès <rouca@debian.org>  Sat, 27 May 2017 16:29:32 +0200

node-static-module (1.3.1-1) unstable; urgency=low

  * Initial release (Closes: #860054)

 -- Bastien Roucariès <rouca@debian.org>  Mon, 10 Apr 2017 23:55:41 +0200
